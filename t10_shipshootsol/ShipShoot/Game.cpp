#include "Game.h"
#include "WindowUtils.h"
#include "CommonStates.h"

#include <windows.h>
#include <string>
#include <cassert>
#include <d3d11.h>
#include <vector>
#include <iomanip>

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

MouseAndKeys Game::sMKIn;
Gamepads Game::sGamepads;

const RECTF coinSpin[]{
	//coin spin
	{ 0, 0, 15, 15},
	{ 16,0,31,15 },
	{ 32,0,47,15 },
	{ 48,0,63,15 },
	{ 0,16,15,31 },
	{ 16,16,31,31 },
	{ 32,16,47,31 },
	{ 48,16,63,31 }
};

const RECTF missileSpin[]{
	{ 0,  0, 53, 48},
	{ 54, 0, 107, 48 },
	{ 108, 0, 161, 48 },
	{ 162, 0, 220, 48 },
};

const RECTF thrustAnim[]{
	{ 0,  0, 15, 16},
	{ 16, 0, 31, 16 },
	{ 32, 0, 47, 16 },
	{ 48, 0, 64, 16 },
};

Game::Game(MyD3D& d3d)
	: mPMode(d3d), mD3D(d3d),  mIMode(d3d), mGOMode(d3d)
{
	sMKIn.Initialise(WinUtil::Get().GetMainWnd(), true, false);
	sGamepads.Initialise();
	mpSB = new SpriteBatch(&mD3D.GetDeviceCtx());
	mpF = new SpriteFont(&mD3D.GetDevice(), L"data/fonts/comicSansMS.spritefont");
	assert(mpF);
}


//any memory or resources we made need releasing at the end
void Game::Release()
{
	delete mpSB;
	mpSB = nullptr;
	
}

//called over and over, use it to update game logic
void Game::Update(float dTime)
{
	sGamepads.Update();
	switch (state)
	{
	case State::START:
		if (Game::sMKIn.IsPressed(VK_SPACE))
		{
			state = State::PLAY;
		}
		break;
	case State::PLAY:
		mPMode.Update(dTime);
	if (mPMode.life = mPMode.life - 1)
	{
		state = State::GAMEOVER;
	}
	break;

	case State::GAMEOVER:
		if (Game::sMKIn.IsPressed(VK_ESCAPE))
		{
			state = State::START;
		}
		break;
	}

	
}

//called over and over, use it to render things
void Game::Render(float dTime)
{
	mD3D.BeginRender(Colours::Black);

	int s = mPMode.score.getAmount();


	CommonStates dxstate(&mD3D.GetDevice());
	mpSB->Begin(SpriteSortMode_Deferred, dxstate.NonPremultiplied(), &mD3D.GetWrapSampler());

	switch (state)
	{
	case State::START:
		mIMode.Render(dTime, *mpSB, *mpF);
		break;
	case State::PLAY:
		mPMode.Render(dTime, *mpSB, s);
		break;
	
	case State::GAMEOVER:
		mGOMode.Render(dTime, *mpSB, *mpF, s);
		break;
	}
	mpSB->End();


	mD3D.EndRender();
	sMKIn.PostProcess();
}

IntroMode::IntroMode(MyD3D& d3d)
	:mD3D(d3d)
{
	Init();

}

void IntroMode::Init()
{
	//a sprite for each layer
	assert(mBgnd.empty());
	mBgnd.insert(mBgnd.begin(), BGND_LAYERS, Sprite(mD3D));

	//a neat way to package pairs of things (nicknames and filenames)
	pair<string, string> files[BGND_LAYERS]{
		{ "bgnd0","backgroundlayers/starfield3.dds" },
		{ "bgnd1","backgroundlayers/starfield2_alpha.dds" },
		{ "bgnd2","backgroundlayers/starfield4.dds" },
		{ "bgnd3","backgroundlayers/TitleScreenTrans.dds" },

	};
	int i = 0;
	for (auto& f : files)
	{
		//set each texture layer
		ID3D11ShaderResourceView* p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), f.second, f.first);
		if (!p)
			assert(false);
		mBgnd[i++].SetTex(*p);
	}

}

void IntroMode::Render(float dTime, DirectX::SpriteBatch& batch, DirectX::SpriteFont& font) {
	for (auto& s : mBgnd)
		s.Draw(batch);

}


//*********************************************************************************
void Asteroid::Init()
{
	vector<RECTF> frames2;
	frames2.insert(frames2.begin(), 8 * 8, RECTF());
	const float ASS_SZ = 102;
	int c = 0;
	for (int y = 0; y < 8; ++y)
		for (int x = 0; x < 8; ++x)
			frames2[c++] = RECTF{ x * ASS_SZ,y * ASS_SZ,x * ASS_SZ + ASS_SZ,y * ASS_SZ + ASS_SZ };
	ID3D11ShaderResourceView* p = spr.GetD3D().GetCache().LoadTexture(&spr.GetD3D().GetDevice(), "asteroid.dds", "asteroid", true, &frames2);

	spr.SetTex(*p);
	spr.GetAnim().Init(0, 31, 15, true);
	spr.GetAnim().Play(true);
	spr.SetScale(Vector2(0.5f, 0.5f));
	spr.origin = Vector2(ASS_SZ / 2.f, ASS_SZ / 2.f);
	active = false;
}

void Asteroid::Render(SpriteBatch& batch)
{
	if (active)
		spr.Draw(batch);
}

void Asteroid::Update(float dTime)
{
	if (active)
	{
		float radius = spr.GetScreenSize().Length() / 2.f;
		spr.mPos.x -= GC::ASTEROID_SPEED * dTime;
		if (spr.mPos.x < -radius)
			active = false;
		spr.GetAnim().Update(dTime);
	}
}

void Bullet::Init(MyD3D& d3d)
{
	vector<RECTF> frames2(missileSpin, missileSpin + sizeof(missileSpin) / sizeof(missileSpin[0]));
	ID3D11ShaderResourceView* p = d3d.GetCache().LoadTexture(&d3d.GetDevice(), "missile.dds", "missile", true, &frames2);

	bullet.SetTex(*p);
	bullet.GetAnim().Init(0, 3, 15, true);
	bullet.GetAnim().Play(true);
	bullet.SetScale(Vector2(0.5f, 0.5f));
	bullet.origin = Vector2((missileSpin[0].right - missileSpin[0].left) / 2.f, (missileSpin[0].bottom - missileSpin[0].top) / 2.f);
	active = false;
}

void Bullet::Render(SpriteBatch& batch)
{
	if (active)
		bullet.Draw(batch);
}

void Bullet::Update(float dTime)
{
	if (active)
	{
		bullet.mPos.x += MISSILE_SPEED * dTime;
		if (bullet.mPos.x > WinUtil::Get().GetClientWidth())
			active = false;
		bullet.GetAnim().Update(dTime);
	}
}

PlayMode::PlayMode(MyD3D & d3d)
	:mD3D(d3d), mPlayer(d3d), mThrust(d3d), mMissile(d3d), mCoinSpin(d3d)
{
	InitBgnd();
	InitPlayer();
	InitRoids();
	InitScore();
	score.Init();
}

void PlayMode::UpdateMissile(float dTime)
{
	if (!mMissile.active && Game::sMKIn.IsPressed(VK_SPACE))
	{
		mMissile.active = true;
		mMissile.bullet.mPos = Vector2(mPlayer.mPos.x + mPlayer.GetScreenSize().x / 2.f, mPlayer.mPos.y);
	}
	mMissile.Update(dTime);
}



void PlayMode::UpdateBgnd(float dTime)
{
	//scroll the background layers
	int i = 0;
	for (auto& s : mBgnd)
		s.Scroll(dTime*(i++)*SCROLL_SPEED, 0);
}

void PlayMode::UpdateInput(float dTime)
{
	Vector2 mouse{ Game::sMKIn.GetMousePos(false) };
	bool keypressed = Game::sMKIn.IsPressed(VK_UP) || Game::sMKIn.IsPressed(VK_DOWN) ||
		Game::sMKIn.IsPressed(VK_RIGHT) || Game::sMKIn.IsPressed(VK_LEFT);
	bool sticked = false;
	if (Game::sGamepads.IsConnected(0) &&
		(Game::sGamepads.GetState(0).leftStickX != 0 || Game::sGamepads.GetState(0).leftStickX != 0))
		sticked = true;

	if (keypressed || (mouse.Length() >VERY_SMALL) || sticked)
	{
		//move the ship around
		Vector2 pos(0, 0);
		if (Game::sMKIn.IsPressed(VK_UP))
			pos.y -= SPEED * dTime;
		else if (Game::sMKIn.IsPressed(VK_DOWN))
			pos.y += SPEED * dTime;
		if (Game::sMKIn.IsPressed(VK_RIGHT))
			pos.x += SPEED * dTime;
		else if (Game::sMKIn.IsPressed(VK_LEFT))
			pos.x -= SPEED * dTime;

		pos += mouse * MOUSE_SPEED * dTime;

		if (sticked)
		{
			DBOUT("left stick x=" << Game::sGamepads.GetState(0).leftStickX << " y=" << Game::sGamepads.GetState(0).leftStickY);
			pos.x += Game::sGamepads.GetState(0).leftStickX * PAD_SPEED * dTime;
			pos.y -= Game::sGamepads.GetState(0).leftStickY * PAD_SPEED * dTime;
		}

		//keep it within the play area
		pos += mPlayer.mPos;
		if (pos.x < mPlayArea.left)
			pos.x = mPlayArea.left;
		else if (pos.x > mPlayArea.right)
			pos.x = mPlayArea.right;
		if (pos.y < mPlayArea.top)
			pos.y = mPlayArea.top;
		else if (pos.y > mPlayArea.bottom)
			pos.y = mPlayArea.bottom;

		mPlayer.mPos = pos;
		mThrusting = GetClock() + 0.2f;
	}
}

void PlayMode::UpdateThrust(float dTime)
{
	if (mThrusting)
	{
		mThrust.mPos = mPlayer.mPos;
		mThrust.mPos.x -= 25;
		mThrust.mPos.y -= 12;
		mThrust.SetScale(Vector2(1.5f, 1.5f));
		mThrust.GetAnim().Update(dTime);
	}
}

void PlayMode::Update(float dTime)
{
	UpdateBgnd(dTime);

	UpdateMissile(dTime);

	UpdateInput(dTime);

	UpdateThrust(dTime);

	UpdateRoids(dTime);

	mCoinSpin.GetAnim().Update(dTime);

}

void PlayMode::Render(float dTime, DirectX::SpriteBatch & batch, int& s) {
	for (auto& s : mBgnd)
		s.Draw(batch);
	if(mThrusting>GetClock())
		mThrust.Draw(batch);
	mMissile.Render(batch);
	mPlayer.Draw(batch);
	RenderRoids(batch);
	mCoinSpin.Draw(batch);
	int displayScore = score.getAmount();

	stringstream ss;
	ss << std::setfill('0') << std::setw(3) << s; // DISPLAY SCORE
	int w, h;
	WinUtil::Get().GetClientExtents(w, h);
	mpFont->DrawString(&batch, ss.str().c_str(), Vector2(w * 0.86f, h * 0.85f), Vector4(1, 1, 1, 1));

}

void PlayMode::InitScore()
{
	vector<RECTF> frames(coinSpin, coinSpin + sizeof(coinSpin) / sizeof(coinSpin[0]));
	ID3D11ShaderResourceView* p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), "coin.dds", "coin", true, &frames);
	mCoinSpin.SetTex(*p);
	mCoinSpin.SetScale(Vector2(2, 2));
	mCoinSpin.GetAnim().Init(0, 7, 15, true);
	int w, h;
	WinUtil::Get().GetClientExtents(w, h);
	mCoinSpin.mPos = Vector2(w * 0.8f, h * 0.85f);
	mCoinSpin.GetAnim().Play(true);
	mpFont = new SpriteFont(&mD3D.GetDevice(), L"data/fonts/comicSansMS.spritefont");
	assert(mpFont);
}

void PlayMode::InitBgnd()
{
	//a sprite for each layer
	//assert(mBgnd.empty());
	mBgnd.insert(mBgnd.begin(), BGND_LAYERS, Sprite(mD3D));

	//a neat way to package pairs of things (nicknames and filenames)
	pair<string, string> files[BGND_LAYERS]{
		{ "bgnd0","backgroundlayers/starfield3.dds" },
		{ "bgnd1","backgroundlayers/starfield2_alpha.dds" },
		{ "bgnd2","backgroundlayers/starfield4.dds" },
	};
	int i = 0;
	for (auto& f : files)
	{
		//set each texture layer
		ID3D11ShaderResourceView *p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), f.second, f.first);
		if (!p)
			assert(false);
		mBgnd[i++].SetTex(*p);
	}

}

void PlayMode::InitPlayer()
{
	//load a orientate the ship
	ID3D11ShaderResourceView *p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), "ship.dds");
	mPlayer.SetTex(*p);
	mPlayer.SetScale(Vector2(0.1f, 0.1f));
	mPlayer.origin = mPlayer.GetTexData().dim / 2.f;
	mPlayer.rotation = PI / 2.f;

	//setup the play area
	int w, h;
	WinUtil::Get().GetClientExtents(w, h);
	mPlayArea.left = mPlayer.GetScreenSize().x*0.6f;
	mPlayArea.top = mPlayer.GetScreenSize().y * 0.6f;
	mPlayArea.right = w - mPlayArea.left;
	mPlayArea.bottom = h * 0.9f;
	mPlayer.mPos = Vector2(mPlayArea.left + mPlayer.GetScreenSize().x / 2.f, (mPlayArea.bottom - mPlayArea.top) / 2.f);

	vector<RECTF> frames(thrustAnim, thrustAnim + sizeof(thrustAnim) / sizeof(thrustAnim[0]));
	p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), "thrust.dds", "thrust", true, &frames);
	mThrust.SetTex(*p);
	mThrust.GetAnim().Init(0, 3, 15, true);
	mThrust.GetAnim().Play(true);
	mThrust.rotation = PI / 2.f;

	mMissile.Init(mD3D);
}

void PlayMode::InitHealth()
{
	ID3D11ShaderResourceView* p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), "ship.dds");

}


Asteroid* PlayMode::CheckCollRoids(Asteroid& me)
{
	assert(!mAsteroids.empty());
	float radius = mAsteroids[0].spr.GetScreenSize().Length() / 2.f;
	size_t i = 0;
	Asteroid* pColl = nullptr;
	while (i < mAsteroids.size() && !pColl)
	{
		Asteroid& collider = mAsteroids[i];
		if ((&me != &collider) &&
			collider.active &&
			(collider.spr.mPos - me.spr.mPos).Length() < (radius * 2))
			pColl = &mAsteroids[i];
		++i;
	}
	return pColl;
}

Asteroid* PlayMode::SpawnRoid()
{
	assert(!mAsteroids.empty());
	size_t i = 0;
	Asteroid* p = nullptr;
	while (i < mAsteroids.size() && !p)
	{
		if (!mAsteroids[i].active)
			p = &mAsteroids[i];
		++i;
	}

	if (p)
	{
		int w, h;
		WinUtil::Get().GetClientExtents(w, h);
		float radius = mAsteroids[0].spr.GetScreenSize().Length() / 2.f;
		Vector2& pos = p->spr.mPos;
		pos.y = (float)GetRandom(radius, h - radius);
		pos.x = (float)(w + radius);
		bool collision = false;
		if (CheckCollRoids(*p))
			collision = true;
		if (!collision)
			p->active = true;
		else
			p = nullptr;
	}
	return p;
}

void PlayMode::UpdateHealth(float dTime)
{

}


void PlayMode::UpdateRoids(float dTime)
{
	int s = score.getAmount();
	assert(!mAsteroids.empty());
	for (auto& a : mAsteroids)
		a.Update(dTime);

	if ((GetClock() - mLastSpawn) > mSpawnRateSec)
	{
		if (SpawnRoid())
			mLastSpawn = GetClock();
	}
	size_t i = 0;
	while (i < mAsteroids.size())
	{
		Asteroid& collider = mAsteroids[i];

		if (collider.active && (collider.spr.mPos - mPlayer.mPos).Length() < 20)
		{
			life = life - 1;
			collider.active = false;
			collider.spr.mPos = Vector2(100, -100);
			return;
		}

		if (collider.active && (collider.spr.mPos - mMissile.bullet.mPos).Length() < 20)
		{
			score.updateAmount(s);
			collider.active = false;
			collider.spr.mPos = Vector2(100, -100);
			mMissile.bullet.mPos = Vector2(100, -100);
			return;
		}
		i++;
	}
}

void PlayMode::InitRoids()
{
	assert(mAsteroids.empty());
	Asteroid a(mD3D);
	a.Init();
	mAsteroids.insert(mAsteroids.begin(), GC::ROID_CACHE, a);
	for (auto& a : mAsteroids)
	{
		if (GetRandom(0, 1) == 0)
			a.spr.GetAnim().Init(0, 31, GetRandom(10.f, 20.f), true);
		else
			a.spr.GetAnim().Init(32, 63, GetRandom(10.f, 20.f), true);
		a.spr.GetAnim().SetFrame(GetRandom(a.spr.GetAnim().GetStart(), a.spr.GetAnim().GetEnd()));
	}
}

void PlayMode::RenderRoids(SpriteBatch& batch)
{
	for (auto& a : mAsteroids)
		a.Render(batch);
}

void Score::Init()
{
	amount_ = 0;
}
int Score::getAmount() const
{
	return amount_;
}
void Score::updateAmount(int value)
//increment when value>0, decrement otherwise
{
	value = 10;
	amount_ += value;
}

GameOverMode::GameOverMode(MyD3D& d3d)
	:mD3D(d3d)
{
	Init();
}

void GameOverMode::Init()
{
	//a sprite for each layer
	assert(mBgnd.empty());
	mBgnd.insert(mBgnd.begin(), BGND_LAYERS, Sprite(mD3D));

	//a neat way to package pairs of things (nicknames and filenames)
	pair<string, string> files[BGND_LAYERS]{
	 { "bgnd0","backgroundlayers/starfield3.dds" },
		{ "bgnd1","backgroundlayers/starfield2_alpha.dds" },
		{ "bgnd2","backgroundlayers/starfield4.dds" },
	};
	int i = 0;
	for (auto& f : files)
	{
		//set each texture layer
		ID3D11ShaderResourceView* p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), f.second, f.first);
		if (!p)
			assert(false);
		mBgnd[i++].SetTex(*p);
	}

}

void GameOverMode::Render(float dTime, DirectX::SpriteBatch& batch, DirectX::SpriteFont& font, int& s)
{
	for (auto& s : mBgnd)
		s.Draw(batch);
	UpdateText(batch, font, s);
}


void GameOverMode::UpdateText(SpriteBatch& batch, SpriteFont& font, int& s)
{

	wstringstream ss;
	ss.precision(3);
	ss << s;
	wstring msg = L"GAME OVER, YOUR SCORE: ";
	msg += ss.str();
	RECT r = font.MeasureDrawBounds(msg.c_str(), Vector2(0, 0));
	Vector2 pos{ 10, WinUtil::Get().GetClientHeight() - r.bottom * 1.1f };
	font.DrawString(&batch, msg.c_str(), pos);
}